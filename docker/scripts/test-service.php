<?php

$result = fillpdf_execute_merge('local_service', ['place' => 'holder'], fillpdf_load(26));

if (!is_string($result)) {
  // Test failed.
  fwrite(STDERR, 'FillPDF Service check failed.' . PHP_EOL);
  return 1;
}

// Test succeeded.
fwrite(STDOUT, 'FillPDF Service check succeeded.' . PHP_EOL);
return 0;
