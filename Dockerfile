FROM ten7/flightdeck-web-7.4:drupal7

USER root

RUN python3 -m ensurepip
RUN python3 -m pip install dnspython

RUN apk add --no-cache tar

USER apache

COPY . /var/www/html

USER root

RUN chmod +w /var/www/html/sites/default && \
  cp /var/www/html/sites/default/settings.dockerfile.php /var/www/html/sites/default/settings.php && \
  chmod -w /var/www/html/sites/default && \
  chown -R apache:apache /var/www/html/

COPY docker/run.yml /ansible/run.yml
COPY docker/dev.yml /ansible/dev.yml

RUN setcap cap_net_bind_service=+ep /usr/sbin/httpd

USER apache

EXPOSE 80 443
