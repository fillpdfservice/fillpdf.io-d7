<?php

/**
 * Implements hook_rules_condition_info().
 */
function fillpdf_service_commerce_rules_condition_info() {
  $conditions['fillpdf_service_commerce_default_profile_address_in_norway'] = array(
    'label' => t('Default customer billing address is in Norway'),
    'group' => t('FillPDF Service Commerce'),
    'callbacks' => array(
      'execute' => 'fillpdf_service_commerce_default_profile_address_in_norway',
    ),
    'parameter' => array(
      'commerce_line_item' => array(
        'type' => 'commerce_line_item',
        'label' => t('line item'),
      ),
    ),
  );

  $conditions['fillpdf_service_commerce_line_item_bound_to_owner_license'] = array(
    'label' => t('Product is used by active customer license'),
    'group' => t('FillPDF Service Commerce'),
    'callbacks' => array(
      'execute' => 'fillpdf_service_commerce_line_item_bound_to_owner_license',
    ),
    'parameter' => array(
      'commerce_line_item' => array(
        'type' => 'commerce_line_item',
        'label' => t('line item'),
      ),
    ),
  );

  $conditions['fillpdf_service_commerce_was_warning_sent'] = array(
    'label' => t('Threshold warning has already been sent'),
    'group' => t('FillPDF Service Commerce'),
    'parameter' => array(
      'license' => array(
        'type' => 'commerce_license',
        'label' => t('FillPDF Service License'),
      ),
      'threshold' => array(
        'type' => 'integer',
        'label' => t('Threshold percentage to check'),
      ),
    ),
  );

  return $conditions;
}

function fillpdf_service_commerce_default_profile_address_in_norway($line_item) {
  // If the line item is defined, our other rule will handle it. Just return
  // FALSE.
  if ($line_item && $line_item->order_id) {
    return FALSE;
  }

  global $user;
  $billing_profile_id = commerce_addressbook_get_default_profile_id($user->uid, 'billing');

  if (empty($billing_profile_id)) {
    return FALSE;
  }

  $billing_profile = commerce_customer_profile_load($billing_profile_id);
  $bpw = entity_metadata_wrapper('commerce_customer_profile', $billing_profile);
  $country = $bpw->commerce_customer_address->country->value();

  if ($country === 'NO') {
    return TRUE;
  }

  return FALSE;
}

/**
 * Provide a condition to avoid the user's license product getting zero-rated
 * by the Rule that ensures disabled products cannot be in the cart. This works
 * around an issue where Commerce License Billing can't properly create the
 * recurring order because a line item with no commerce_unit_price (null amount)
 * fails to be inserted to the database.
 *
 * @param $line_item
 * @return bool
 */
function fillpdf_service_commerce_line_item_bound_to_owner_license($line_item) {
  $wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);
  if (isset($wrapper->commerce_product)) {
    $line_item_product_id = $wrapper->commerce_product->getIdentifier();

    $license = fillpdf_service_commerce_check_license($wrapper->order->owner->value());
    if (empty($license->error)) {
      $license_product_id = $license->product_id;

      if ($license_product_id === $line_item_product_id) {
        return TRUE;
      }
    }
  }
  return FALSE;
}

/**
 * Callback for Rules condition.
 *
 * @param int $threshold
 *   The threshold percentage to check for, e.g. 75.
 */
function fillpdf_service_commerce_was_warning_sent($license, $threshold) {
  /** @var \CommerceLicenseBillingCycle $current_billing_cycle */
  $current_billing_cycle = commerce_license_billing_get_license_billing_cycle($license);

  $message_efq = new EntityFieldQuery();
  $result = $message_efq->entityCondition('entity_type', 'message')
    ->entityCondition('bundle', 'usage_nearing_threshold')
    ->fieldCondition('field_billing_cycle_id', 'value', $current_billing_cycle->billing_cycle_id)
    ->fieldCondition('field_threshold', 'value', $threshold)
    ->execute();

  if (empty($result['message'])) {
    return FALSE;
  }

  return TRUE;
}

/**
 * Implements hook_rules_action_info().
 */
function fillpdf_service_commerce_rules_action_info() {
  return array(
    'fillpdf_service_commerce_load_license_billing_cycle' => array(
      'label' => t('Load the current billing cycle for a license'),
      'group' => t('FillPDF Service Commerce'),
      'parameter' => array(
        'license' => array(
          'type' => 'commerce_license',
          'label' => t('License'),
        ),
      ),
      'provides' => array(
        'billing_cycle' => array(
          'type' => 'cl_billing_cycle',
          'label' => t('Current license billing cycle'),
        ),
      ),
    ),
  );
}

/**
 * @param \CommerceLicenseInterface $license
 *   The user license for which the billing cycle should be loaded.
 */
function fillpdf_service_commerce_load_license_billing_cycle($license) {
  return array(
    'billing_cycle' => commerce_license_billing_get_license_billing_cycle($license)
  );
}
