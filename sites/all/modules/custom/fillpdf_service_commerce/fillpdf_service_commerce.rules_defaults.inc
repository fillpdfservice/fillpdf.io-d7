<?php
/**
 * @file
 * fillpdf_service_commerce_customizations.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function fillpdf_service_commerce_default_rules_configuration() {
  $items = array();
  $items['rules_75_merge_warning'] = entity_import('rules_config', '{ "rules_75_merge_warning" : {
      "LABEL" : "75% Merge Warning",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "FillPDF Service" ],
      "REQUIRES" : [
        "rules",
        "fillpdf_service_commerce",
        "message_notify",
        "fillpdf_service"
      ],
      "ON" : { "fillpdf_service_post_merge" : [] },
      "IF" : [
        { "NOT data_is" : { "data" : [ "user:field-disable-usage-warnings" ], "value" : "1" } },
        { "data_is" : {
            "data" : [ "user:field-fps-merges-percentage" ],
            "op" : "\\u003E",
            "value" : "0.7499"
          }
        },
        { "NOT fillpdf_service_commerce_was_warning_sent" : { "license" : [ "license" ], "threshold" : "75" } }
      ],
      "DO" : [
        { "fillpdf_service_commerce_load_license_billing_cycle" : {
            "USING" : { "license" : [ "license" ] },
            "PROVIDE" : { "billing_cycle" : { "billing_cycle" : "Current license billing cycle" } }
          }
        },
        { "entity_create" : {
            "USING" : {
              "type" : "message",
              "param_type" : "usage_nearing_threshold",
              "param_user" : [ "user" ]
            },
            "PROVIDE" : { "entity_created" : { "message" : "Threshold message" } }
          }
        },
        { "data_set" : {
            "data" : [ "message:field-billing-cycle-id" ],
            "value" : [ "billing-cycle:billing-cycle-id" ]
          }
        },
        { "data_set" : { "data" : [ "message:field-threshold" ], "value" : "75" } },
        { "message_notify_process" : { "message" : [ "message" ] } }
      ]
    }
  }');
  $items['rules_90_merge_warning'] = entity_import('rules_config', '{ "rules_90_merge_warning" : {
      "LABEL" : "90% Merge Warning",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "FillPDF Service" ],
      "REQUIRES" : [
        "rules",
        "fillpdf_service_commerce",
        "message_notify",
        "fillpdf_service"
      ],
      "ON" : { "fillpdf_service_post_merge" : [] },
      "IF" : [
        { "NOT data_is" : { "data" : [ "user:field-disable-usage-warnings" ], "value" : "1" } },
        { "data_is" : {
            "data" : [ "user:field-fps-merges-percentage" ],
            "op" : "\\u003E",
            "value" : "0.8999"
          }
        },
        { "NOT fillpdf_service_commerce_was_warning_sent" : { "license" : [ "license" ], "threshold" : "90" } }
      ],
      "DO" : [
        { "fillpdf_service_commerce_load_license_billing_cycle" : {
            "USING" : { "license" : [ "license" ] },
            "PROVIDE" : { "billing_cycle" : { "billing_cycle" : "Current license billing cycle" } }
          }
        },
        { "entity_create" : {
            "USING" : {
              "type" : "message",
              "param_type" : "usage_nearing_threshold",
              "param_user" : [ "user" ]
            },
            "PROVIDE" : { "entity_created" : { "message" : "Threshold message" } }
          }
        },
        { "data_set" : {
            "data" : [ "message:field-billing-cycle-id" ],
            "value" : [ "billing-cycle:billing-cycle-id" ]
          }
        },
        { "data_set" : { "data" : [ "message:field-threshold" ], "value" : "90" } },
        { "message_notify_process" : { "message" : [ "message" ] } }
      ]
    }
  }');
  return $items;
}

/**
 * Implements hook_default_rules_configuration_alter().
 */
function fillpdf_service_commerce_default_rules_configuration_alter(&$configs) {
  $configs['commerce_vat_place_of_supply_si'] = entity_import(
    'rules_config',
    '{ "commerce_vat_place_of_supply_si" : {
    "LABEL" : "Place of Supply is in Slovenia",
    "PLUGIN" : "or",
    "OWNER" : "rules",
    "TAGS" : [ "Commerce VAT", "Place of Supply" ],
    "REQUIRES" : [ "rules" ],
    "USES VARIABLES" : { "line_item" : { "label" : "Commerce Line Item", "type" : "commerce_line_item" } },
    "OR" : [
      { "AND" : [
          { "entity_has_field" : {
              "entity" : [ "line-item:order" ],
              "field" : "commerce_customer_shipping"
            }
          },
          { "NOT data_is_empty" : { "data" : [ "line-item:order:commerce-customer-shipping" ] } },
          { "component_commerce_vat_profile_address_si" : { "customer_profile" : [ "line-item:order:commerce-customer-shipping" ] } }
        ]
      },
      { "AND" : [
          { "entity_has_field" : {
              "entity" : [ "line-item:order" ],
              "field" : "commerce_customer_billing"
            }
          },
          { "NOT data_is_empty" : { "data" : [ "line-item:order:commerce-customer-billing" ] } },
          { "component_commerce_vat_profile_address_si" : { "customer_profile" : [ "line-item:order:commerce-customer-billing" ] } }
        ]
      }
    ]
  }
}'
  );
}
