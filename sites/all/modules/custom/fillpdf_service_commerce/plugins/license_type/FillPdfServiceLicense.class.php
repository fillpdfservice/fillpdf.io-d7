<?php

/**
 * Custom license type for FillPDF Service subscriptions.
 */
class FillPdfServiceLicense extends CommerceLicenseRole implements CommerceLicenseBillingUsageInterface {

  /**
   * Implements CommerceLicenseInterface::isConfigurable().
   */
  public function isConfigurable() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function usageDetails() {
    $groups = $this->usageGroups();

    return 'FillPDF Service usage placeholder';
  }

  /**
   * {@inheritdoc}
   */
  public function usageGroups() {
    $usage_groups = array();

    // First, find overage products.
    $query = new EntityFieldQuery();
    $result = $query->entityCondition('entity_type', 'commerce_product')
      ->entityCondition('bundle', 'fillpdf_service_usage')
      ->execute();

    $usage_products = array();
    if (!empty($result['commerce_product'])) {
      $usage_products = entity_load('commerce_product', array_keys($result['commerce_product']));
    }

    foreach ($usage_products as $usage_product) {
      // Great, so we have an overage product. We essentially want
      // a usage group for every product that references *this* one
      // (because we need to figure out free quantity, etc.).
      $subquery = new EntityFieldQuery();
      $subresult = $subquery->entityCondition('entity_type', 'commerce_product')
        ->entityCondition('bundle', 'product')
        ->fieldCondition('field_overage_product', 'product_id', $usage_product->product_id)
        ->execute();

      $usage_enabled_products = array();
      if (!empty($subresult['commerce_product'])) {
        $usage_enabled_products = entity_load('commerce_product', array_keys($subresult['commerce_product']));
      }

      foreach ($usage_enabled_products as $uep) {
        // Compose a unique SKU based on the SKU of the product the customer
        // buys and the SKU of the overage product (several products might
        // use the same overage product).
        $ugkey = "{$uep->sku}_{$usage_product->sku}";

        // And, finally, add a usage group with that key.
        $uepw = entity_metadata_wrapper('commerce_product', $uep);
        $uep_free_quantity = $uepw->field_fillpdf_service_merges->value();
        $usage_groups[$ugkey] = array(
          // Track PDF merges.
          'title' => t('Populate API calls'),
          'type' => 'fps_counter',
          'product' => $usage_product->sku,
          'free_quantity' => $uep_free_quantity,
          // We report all API usage immediately, so the customer can be billed
          // at their billing cycle freely.
          'immediate' => TRUE,
        );
      }
    }

    return $usage_groups;
  }

}
