<?php

/**
 * @file
 * Provides a license type plugin.
 */

$plugin = array(
  'title' => t('FillPDF Service Subscription'),
  'class' => 'FillPdfServiceLicense',
);
