<?php

/**
 * @file
 * Provides a counter usage group plugin.
 */

$plugin = array(
  'title' => t('FillPDF Service Counter'),
  'class' => 'FillPdfServiceCounterUsageGroup',
);
