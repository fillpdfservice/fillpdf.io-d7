<?php
/**
 * Allows handling free usage differently, since plan upgrades can happen
 * within a billing cycle.
 */
class FillPdfServiceCounterUsageGroup extends CommerceLicenseBillingCounterUsageGroup {

  public function chargeableUsage(CommerceLicenseBillingCycle $billingCycle) {
    $chargeable_usage = array();
    $usage = $this->usageHistory($billingCycle);
    $free_quantities = $this->freeQuantities($billingCycle);

    $current_revision = $this->license->revision_id;
    $current_free_quantity = NULL;
    if (!empty($usage)) {
      // Add a placeholder record for the current revision if we don't have one.
      // This is for when they have, for example, just upgraded.
      if (in_array((int) $current_revision, array_keys($free_quantities)) === FALSE) {
        $overage_group = fillpdf_service_commerce_metered_usage_group($this->license);
        $free_quantity = $overage_group->groupInfo['free_quantity'];
        $free_quantities[$current_revision] = array(
          'quantity' => $free_quantity,
          'start' => $this->license->revision_created,
          'end' => $this->license->revision_ended,
        );
      }

      $current_free_quantity = $free_quantities[$current_revision];
    }

    // There could be multiple records per revision, so group them first.
    $counter_totals = array();
    foreach ($usage as $index => $usage_record) {
      $revision_id = $usage_record['revision_id'];
      // Initialize the counter.
      if (!isset($counter_totals[$revision_id])) {
        $counter_totals[$revision_id] = 0;
      }
      $counter_totals[$revision_id] += $usage_record['quantity'];
    }
    // Now compare the totals with the free quantities for each revision, and
    // create the final total that has only the non-free quantities.
    $total = 0;
    foreach ($counter_totals as $revision_id => $quantity) {
      // Use the most generous free quantity - the revision or the current one.
      // This is so that upgrades blow away earlier metered usage and so that
      // downgrades don't incorrectly add too much metered usage.
      $free_quantity_amount = max($free_quantities[$revision_id]['quantity'], $current_free_quantity['quantity']);

      if ($quantity > $free_quantity_amount) {
        $total += ($quantity - $free_quantity_amount);
      }
    }

    if ($total > 0) {
      $chargeable_usage[] = array(
        'usage_group' => $this->groupName,
        'quantity' => $total,
        'start' => $billingCycle->start,
        'end' => $billingCycle->end,
      );
    }

    return $chargeable_usage;
  }

}