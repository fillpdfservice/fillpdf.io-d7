<?php

class FillPdfServiceApiKeyMigration extends Migration {

  public function __construct($arguments) {
    parent::__construct($arguments);

    $this->description = t('Migrate FillPDF Service API keys to field_api_key on users.');

    // We are updating existing content in Drupal.
    $this->systemOfRecord = Migration::DESTINATION;

    // We need to select uid and api_key from fillpdf_support. We need uid in
    // order to map API keys to previously-imported users.
    // Expects $databases['drupal6']['default'] in settings.php.
    $query = Database::getConnection('default', 'drupal6')
      ->select('fillpdf_support', 'fs')
      ->fields('fs', array('uid', 'api_key'));

    $this->source = new MigrateDrupal6SourceSQL($query, array(), NULL, array('fix_field_names' => array()));
    $this->destination = new MigrateDestinationUser();
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'uid' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'description' => 'ID of destination user',
          'not null' => TRUE,
        ),
      ),
      MigrateDestinationUser::getKeySchema(),
      'drupal6'
    );

    $source_migration = variable_get('commerce_migrate_ubercart_user_migration_class', "");
    $this->addFieldMapping('uid', 'uid')
      ->sourceMigration($source_migration)
      ->defaultValue(0);
    $this->addFieldMapping('field_api_key', 'api_key');
  }

}

class FillPdfServiceFreeUserMigration extends Migration {

  public function __construct(array $arguments) {
    parent::__construct($arguments);

    // Select data that we need in order to grant proper licenses.
    // Expects $databases['drupal6']['default'] in settings.php.
    $fps_role = Database::getConnection('default', 'drupal6')
      ->query("select rid from {role} where name = 'FillPDF License'")
      ->fetchField();

    // Base table is fillpdf_support; we will not migrate more than one license
    // per user.
    $query = Database::getConnection('default', 'drupal6')
      ->select('fillpdf_support', 'fs')
      ->fields('fs', array('uid'));

    // Filter by users that actually have the FillPDF License role
    // (subscribed users).
    $query->join('users_roles', 'ur', 'ur.uid = fs.uid AND ur.rid = :fps_role', array(':fps_role' => $fps_role));

    // And get the nid to which the vid belongs.
    $query->join('node_revisions', 'n', 'n.vid = fs.vid');

    // We want to filter out free products.
    $query->join('uc_products', 'ucp', 'ucp.vid = n.vid and ucp.sell_price > 0.0000');

    $paid_d6_users = $query->execute()->fetchCol();

    // Free users are users that are NOT paid users :)
    $source_query = Database::getConnection('default', 'drupal6')
      ->select('users', 'u')
      ->fields('u', array('uid'))
      ->condition('u.uid', $paid_d6_users, 'NOT IN');

    $this->source = new MigrateDrupal6SourceSQL($source_query, array(), NULL, array('fix_field_names' => array()));
    $this->destination = new MigrateDestinationCommerceLicense('commerce_license', 'fillpdf_service');
    $this->map = new MigrateSQLMap($this->machineName,
      array(
        'uid' => array(
          'type' => 'int',
          'unsigned' => TRUE,
          'description' => 'ID of destination user',
          'not null' => TRUE,
        ),
      ),
      MigrateDestinationCommerceLicense::getKeySchema('commerce_license'),
      'drupal6'
    );

    $source_migration = variable_get('commerce_migrate_ubercart_user_migration_class', "");
    $this->addFieldMapping('uid', 'uid')
      ->sourceMigration($source_migration)
      ->defaultValue(0);

    $this->addFieldMapping('product_id')
      ->defaultValue(1)
      ->sourceMigration('CommerceMigrateUbercartProductProduct')
      ->callbacks(array($this, 'getProductId'));

    $this->addFieldMapping('status')
      ->defaultValue(COMMERCE_LICENSE_ACTIVE);

    // owner and product, while listed by Commerce License, don't seem to
    // actually work.
    $this->addUnmigratedDestinations(array(
      'num_renewals',
      'cle_name',
      'path',
      'owner',
      'product'
    ));

    // Mark licenses as being granted at migration run time.
    $this->addFieldMapping('granted')
      ->defaultValue(REQUEST_TIME);

    // And expiring sixty days later.
    $sixty_days = 24 * 3600 * 60;
    $this->addFieldMapping('expires')
      ->defaultValue(REQUEST_TIME + $sixty_days);
  }

  // Get just the entity ID from the source.
  protected function getProductId($product_ids) {
    if (!empty($product_ids['destid1'])) {
      return $product_ids['destid1'];
    }
    return $product_ids;
  }

}

/**
 * Subclasses the line item migration so that we can do extra things in
 * complete():
 *
 * - Activate licenses that correspond to a user's most recent order.
 * - Mark all other licenses as expired. They will be linked to the order, but
 *   they will not work. As it should be.
 * - Create Commerce License Billing recurring orders for the active licenses.
 */
class FillPdfServiceSubscriberMigration extends CommerceMigrateUbercartLineItem {

  /**
   * Called from MigrateDestinationCommerceLicense::complete().
   *
   * Add an order with this license as a line item so that Commerce License
   * Billing will set up recurring orders.
   *
   * @param $license_id
   */
  public function complete($line_item, stdClass $source_row) {
    parent::complete($line_item, $source_row);

    static $paid_d6_users = array();

    if (empty($paid_d6_users)) {
      // Select data that we need in order to grant proper licenses.
      // Expects $databases['drupal6']['default'] in settings.php.
      $fps_role = Database::getConnection('default', 'drupal6')
        ->query("select rid from {role} where name = 'FillPDF License'")
        ->fetchField();

      // Base table is fillpdf_support; we will not migrate more than one license
      // per user.
      $query = Database::getConnection('default', 'drupal6')
        ->select('fillpdf_support', 'fs')
        ->fields('fs', array('uid'));

      // Filter by users that actually have the FillPDF License role
      // (subscribed users).
      $query->join('users_roles', 'ur', 'ur.uid = fs.uid AND ur.rid = :fps_role', array(':fps_role' => $fps_role));

      // And get the nid to which the vid belongs.
      $query->join('node_revisions', 'n', 'n.vid = fs.vid');

      // We want to filter out free products.
      $query->join('uc_products', 'ucp', 'ucp.vid = n.vid and ucp.sell_price > 0.0000');

      $paid_d6_users = $query->execute()->fetchCol();
    }

    // Map the D6 user IDs to D7 user IDs.
    static $paid_d7_users = array();
    static $d7_d6_map = array();
    if (empty($paid_d7_users)) {
      $user_migration = strtolower(variable_get('commerce_migrate_ubercart_user_migration_class', ""));
      // If we don't have a user migration, then the D7 paid users array will
      // just be empty and nothing will happen (well, all licenses will get
      // expired).
      if ($user_migration) {
        foreach ($paid_d6_users as $paid_d6_user) {
          // Find the mapped D7 user and add it to the array.
          $d7_user = db_query("select destid1
          from {migrate_map_{$user_migration}}
          where sourceid1 = :d6_uid",
            array(':d6_uid' => $paid_d6_user))
            ->fetchField();

          if ($d7_user) {
            $paid_d7_users[$paid_d6_user] = $d7_user;
            $d7_d6_map[$d7_user] = $paid_d6_user;
          }
        }
      }
    }

    $wrapper = entity_metadata_wrapper('commerce_line_item', $line_item);

    // We initialize these here to avoid empty object reference errors later
    // when the commerce_license on the line item is empty.
    $license_wrapper = $license = NULL;
    if (isset($wrapper->commerce_license)) {
      $license_wrapper = $wrapper->commerce_license;

      if ($license_wrapper) {
        /** @var CommerceLicenseInterface $license */
        $license = $wrapper->commerce_license->value();
      }
    }

    // Always mark expired unless they're a paying user.
    if (in_array($wrapper->order->uid->value(), $paid_d7_users)) {
      // Load the customer's latest order.
      $eq = new EntityFieldQuery();
      $cor = $eq->entityCondition('entity_type', 'commerce_order')
        ->propertyCondition('uid', $license_wrapper->owner->uid->value())
        ->propertyOrderBy('created', 'DESC')
        ->range(0, 1)
        ->execute();

      if (!empty($cor['commerce_order'])) {
        // I should cache this, but meh.
        $commerce_order_keys = array_keys($cor['commerce_order']);
        $latest_order = entity_load_single('commerce_order', end($commerce_order_keys));
        $order_wrapper = entity_metadata_wrapper('commerce_order', $latest_order);

        // If the order we are handling is the same as this one, then activate
        // the license.
        if ($order_wrapper->getIdentifier() === $wrapper->order->getIdentifier()) {
          $license->activate();

          // Set the granted time; Commerce License Billing will set expiration
          // when creating the recurring order.
          $license->granted = commerce_license_get_time();
          $license->save();

          // Does the license product actually ever expire? If not, we don't want
          // to create a recurring order.
          if (isset($license_wrapper->product->commerce_license_duration)) {
            $duration = (int) $license_wrapper->product->commerce_license_duration->value();
            if ($duration) {
              commerce_license_billing_create_recurring_orders($latest_order);

              // If we know their expiration date from D6, update the license
              // to use it.
              if (!empty($d7_d6_map[$license->uid])) {
                $license = entity_load_single('commerce_license', $license->license_id);
                $expiration = Database::getConnection('default', 'drupal6')
                  ->select('uc_roles_expirations', 'ucr')
                  ->fields('ucr', array('expiration'))
                  ->condition('uid', $d7_d6_map[$license->uid])
                  ->execute()
                  ->fetchField();

                if ($expiration) {
                  // Set the expiration to 7 days if they don't have at least 7 days remaining.
                  $expiration = max($expiration, commerce_license_get_time() + (3600 * 24 * 7));

                  // Ensure the first billing cycle that got created has the same end time as the license expiration.
                  $billing_cycle = commerce_license_billing_get_license_billing_cycle($license);
                  $billing_cycle->end = $expiration;
                  $billing_cycle->save();

                  $license->expires = $expiration;
                  $license->wrapper->save();
                }
              }
            }
          }
        }
      }
      else {
        $this->saveMessage(t('Could not any orders for user @uid. Did not do anything. This message might appear more than once.',
          array('@uid' => $line_item->uid)),
          MigrationBase::MESSAGE_WARNING);
      }
    }

    if ($license && (int) $license->status !== COMMERCE_LICENSE_ACTIVE) {
      $license->expire();
    }
  }

}
