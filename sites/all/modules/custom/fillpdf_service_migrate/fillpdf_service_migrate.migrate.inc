<?php
/**
 * Implements hook_migrate_api().
 */
function fillpdf_service_migrate_migrate_api() {
  $api = array(
    'api' => 2,

    'groups' => array(
      'fillpdf_service_subscriptions' => array(
        'title' => t('FillPDF Service Subscription'),
      ),
    ),

    'migrations' => array(
      'FillPdfServiceApiKey' => array(
        'class_name' => 'FillPdfServiceApiKeyMigration',
        'group_name' => 'fillpdf_service_subscriptions',
      ),

      'FillPdfServiceFreeUser' => array(
        'class_name' => 'FillPdfServiceFreeUserMigration',
        'group_name' => 'fillpdf_service_subscriptions',
      ),
    ),
  );

  return $api;
}

/**
 * Implements hook_migrate_api_alter().
 */
function fillpdf_service_migrate_migrate_api_alter(array &$info) {
  $info['commerce_migrate_ubercart']['migrations']['CommerceMigrateUbercartLineItem']['class_name'] = 'FillPdfServiceSubscriberMigration';
}
