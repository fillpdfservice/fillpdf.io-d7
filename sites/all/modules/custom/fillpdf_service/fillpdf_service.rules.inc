<?php
/**
 * Implements hook_rules_event_info().
 */
function fillpdf_service_rules_event_info() {
  return array(
    'fillpdf_service_pre_merge' => array(
      'label' => t('Before handling a merge request'),
      'group' => t('FillPDF Service'),
      'variables' => fillpdf_service_rules_event_variables(),
    ),
    'fillpdf_service_post_merge' => array(
      'label' => t('After handling a merge request'),
      'group' => t('FillPDF Service'),
      'variables' => fillpdf_service_rules_event_variables(),
    ),
  );
}

/**
 * @return array
 */
function fillpdf_service_rules_event_variables() {
  return array(
    'user' => array(
      'label' => t('User'),
      'type' => 'user',
    ),
    'license' => array(
      'label' => t('FillPDF Service License'),
      'type' => 'commerce_license',
    ),
  );
}
