<?php

/**
 * @file fillpdf_service.api.php
 * Hooks provided by the FillPDF Service module.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Allow modules to act before the merge actually occurs but after we have
 * validated the user account (and, if applicable, license).
 *
 * @param $account
 *  The user account.
 * @param $context
 *  An array that's like this by default:
 *  @code
 *    array(
 *      // If available.
 *      'license' => (object of type CommerceLicenseInterface)
 *    )
 *  @endcode
 *
 */
function hook_fillpdf_service_pre_merge($account, $context) {

}

/**
 * Allow modules to act after the service has merged the PDF (and, if
 * applicable, debited usage from the user's licensed allowance).
 *
 * @param $account
 *  The user account.
 * @param $context
 *  An array that's like this by default:
 *  @code
 *    array(
 *      // If available.
 *      'license' => (object of type CommerceLicenseInterface)
 *    )
 *  @endcode
 *
 */
function hook_fillpdf_service_post_merge($account, $context) {
  $license = $context['license'];
  if (empty($license->error)) {
    fillpdf_service_recompute_entity_field('user', $account, 'field_fps_merges_used');
  }
}
/**
 * @}
 */
