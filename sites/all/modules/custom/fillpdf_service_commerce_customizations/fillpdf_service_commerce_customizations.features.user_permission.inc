<?php
/**
 * @file
 * fillpdf_service_commerce_customizations.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function fillpdf_service_commerce_customizations_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer card data'.
  $permissions['administer card data'] = array(
    'name' => 'administer card data',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'commerce_cardonfile',
  );

  // Exported permission: 'configure cardonfile'.
  $permissions['configure cardonfile'] = array(
    'name' => 'configure cardonfile',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'commerce_cardonfile',
  );

  // Exported permission: 'create any card data'.
  $permissions['create any card data'] = array(
    'name' => 'create any card data',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'commerce_cardonfile',
  );

  // Exported permission: 'create field_api_key'.
  $permissions['create field_api_key'] = array(
    'name' => 'create field_api_key',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_vat_number'.
  $permissions['create field_vat_number'] = array(
    'name' => 'create field_vat_number',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create own card data'.
  $permissions['create own card data'] = array(
    'name' => 'create own card data',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'commerce_cardonfile',
  );

  // Exported permission: 'delete any card data'.
  $permissions['delete any card data'] = array(
    'name' => 'delete any card data',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'commerce_cardonfile',
  );

  // Exported permission: 'delete own card data'.
  $permissions['delete own card data'] = array(
    'name' => 'delete own card data',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'commerce_cardonfile',
  );

  // Exported permission: 'edit any card data'.
  $permissions['edit any card data'] = array(
    'name' => 'edit any card data',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'commerce_cardonfile',
  );

  // Exported permission: 'edit field_api_key'.
  $permissions['edit field_api_key'] = array(
    'name' => 'edit field_api_key',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_vat_number'.
  $permissions['edit field_vat_number'] = array(
    'name' => 'edit field_vat_number',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own card data'.
  $permissions['edit own card data'] = array(
    'name' => 'edit own card data',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'commerce_cardonfile',
  );

  // Exported permission: 'edit own field_api_key'.
  $permissions['edit own field_api_key'] = array(
    'name' => 'edit own field_api_key',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_vat_number'.
  $permissions['edit own field_vat_number'] = array(
    'name' => 'edit own field_vat_number',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view any card data'.
  $permissions['view any card data'] = array(
    'name' => 'view any card data',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'commerce_cardonfile',
  );

  // Exported permission: 'view field_api_key'.
  $permissions['view field_api_key'] = array(
    'name' => 'view field_api_key',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_vat_number'.
  $permissions['view field_vat_number'] = array(
    'name' => 'view field_vat_number',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own card data'.
  $permissions['view own card data'] = array(
    'name' => 'view own card data',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'commerce_cardonfile',
  );

  // Exported permission: 'view own field_api_key'.
  $permissions['view own field_api_key'] = array(
    'name' => 'view own field_api_key',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_vat_number'.
  $permissions['view own field_vat_number'] = array(
    'name' => 'view own field_vat_number',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
