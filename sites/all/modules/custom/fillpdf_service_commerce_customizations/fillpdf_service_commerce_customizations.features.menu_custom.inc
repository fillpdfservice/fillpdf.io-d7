<?php
/**
 * @file
 * fillpdf_service_commerce_customizations.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function fillpdf_service_commerce_customizations_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-fps-social-links.
  $menus['menu-fps-social-links'] = array(
    'menu_name' => 'menu-fps-social-links',
    'title' => 'FPS Social Links',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('FPS Social Links');

  return $menus;
}
