<?php
/**
 * @file
 * fillpdf_service_commerce_customizations.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function fillpdf_service_commerce_customizations_user_default_roles() {
  $roles = array();

  // Exported role: Hosted FillPDF.
  $roles['Hosted FillPDF'] = array(
    'name' => 'Hosted FillPDF',
    'weight' => 7,
  );

  return $roles;
}
