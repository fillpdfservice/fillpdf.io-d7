<?php
/**
 * @file
 * fillpdf_service_commerce_customizations.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function fillpdf_service_commerce_customizations_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'FillPDF Service Footer';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'fps_footer';
  $fe_block_boxes->body = '&copy; 2010-[current-date:custom:Y] Kaland Web (Organization number: NO 999 052 835 MVA). Marketed as FillPDF Service. All rights reserved. Diagram on homepage provided by the talented <a href="http://www.barrettmorgandesignllc.com/">Barrett Morgan Design LLC</a>.

This site interfaces with an iText application. You can find, review, and download the source code of that application <a href="https://github.com/fillpdfservice/fillpdf-servlet">on GitHub</a>.

You can review or fork the source code of the website <a href="https://gitlab.com/fillpdfservice/fillpdf.io-d7">here</a>. Remember that FillPDF, FillPDF Service, and the FillPDF Service logo are trademarks of Kaland Web.

iText and com.ocdevel.FillpdfService are licensed under the GNU Affero General Public License Version 3 or later. The text can be found <a href="http://itextpdf.com/terms-of-use/agpl.php">here</a>.

The website is licensed under the GNU General Public License Version 3 or later.';

  $export['fps_footer'] = $fe_block_boxes;

  return $export;
}
