<?php
/**
 * @file
 * fillpdf_service_commerce_customizations.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function fillpdf_service_commerce_customizations_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-fps-social-links_facebook-fillpdfservice:https://www.facebook.com/FillPDFService.
  $menu_links['menu-fps-social-links_facebook-fillpdfservice:https://www.facebook.com/FillPDFService'] = array(
    'menu_name' => 'menu-fps-social-links',
    'link_path' => 'https://www.facebook.com/FillPDFService',
    'router_path' => '',
    'link_title' => 'Facebook: @FillPDFService',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-fps-social-links_facebook-fillpdfservice:https://www.facebook.com/FillPDFService',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: menu-fps-social-links_twitter-fillpdf:https://twitter.com/FillPDF.
  $menu_links['menu-fps-social-links_twitter-fillpdf:https://twitter.com/FillPDF'] = array(
    'menu_name' => 'menu-fps-social-links',
    'link_path' => 'https://twitter.com/FillPDF',
    'router_path' => '',
    'link_title' => 'Twitter: @FillPDF',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'menu-fps-social-links_twitter-fillpdf:https://twitter.com/FillPDF',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Facebook: @FillPDFService');
  t('Twitter: @FillPDF');

  return $menu_links;
}
