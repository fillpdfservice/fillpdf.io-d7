<?php
/**
 * @file
 * fillpdf_service_commerce_customizations.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function fillpdf_service_commerce_customizations_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_api_details|user|user|default';
  $field_group->group_name = 'group_api_details';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Click to view API details',
    'weight' => '0',
    'children' => array(
      0 => 'field_api_key',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Click to view API details',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-api-details field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups[''] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_api_details|user|user|form';
  $field_group->group_name = 'group_api_details';
  $field_group->entity_type = 'user';
  $field_group->bundle = 'user';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'API details',
    'weight' => '6',
    'children' => array(
      0 => 'field_api_key',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-api-details field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups[''] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('API details');
  t('Click to view API details');

  return $field_groups;
}
