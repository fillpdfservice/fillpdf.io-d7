<?php
/**
 * @file
 * fillpdf_service_commerce_customizations.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function fillpdf_service_commerce_customizations_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['block-fps_footer'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'fps_footer',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adaptivetheme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adaptivetheme',
        'weight' => 0,
      ),
      'at_commerce' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'at_commerce',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'fps' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'fps',
        'weight' => -24,
      ),
      'omega_kickstart' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'omega_kickstart',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['commerce_cart-cart'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'cart',
    'module' => 'commerce_cart',
    'node_types' => array(),
    'pages' => 'checkout*',
    'roles' => array(),
    'themes' => array(
      'adaptivetheme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adaptivetheme',
        'weight' => 0,
      ),
      'at_commerce' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'at_commerce',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'fps' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'fps',
        'weight' => -3,
      ),
      'omega_kickstart' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'omega_kickstart',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['commerce_checkout_progress-indication'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'indication',
    'module' => 'commerce_checkout_progress',
    'node_types' => array(),
    'pages' => 'checkout/*
cart',
    'roles' => array(),
    'themes' => array(
      'adaptivetheme' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'adaptivetheme',
        'weight' => -10,
      ),
      'at_commerce' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'at_commerce',
        'weight' => -10,
      ),
      'bartik' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'bartik',
        'weight' => -10,
      ),
      'fps' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'fps',
        'weight' => -23,
      ),
      'omega_kickstart' => array(
        'region' => 'breadcrumb',
        'status' => 1,
        'theme' => 'omega_kickstart',
        'weight' => -10,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  $export['commerce_kickstart_block-powered_drupal_commerce'] = array(
    'cache' => 1,
    'custom' => 0,
    'delta' => 'powered_drupal_commerce',
    'module' => 'commerce_kickstart_block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adaptivetheme' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'adaptivetheme',
        'weight' => -20,
      ),
      'at_commerce' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'at_commerce',
        'weight' => -20,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'fps' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'fps',
        'weight' => -11,
      ),
      'omega_kickstart' => array(
        'region' => 'footer2_first',
        'status' => 1,
        'theme' => 'omega_kickstart',
        'weight' => -20,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['commerce_kickstart_block-proudly_built_by_cg'] = array(
    'cache' => 1,
    'custom' => 0,
    'delta' => 'proudly_built_by_cg',
    'module' => 'commerce_kickstart_block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adaptivetheme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adaptivetheme',
        'weight' => 0,
      ),
      'at_commerce' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'at_commerce',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'fps' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'fps',
        'weight' => -10,
      ),
      'omega_kickstart' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'omega_kickstart',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['menu-menu-fps-social-links'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'menu-fps-social-links',
    'module' => 'menu',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adaptivetheme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adaptivetheme',
        'weight' => 0,
      ),
      'at_commerce' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'at_commerce',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'fps' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'fps',
        'weight' => -25,
      ),
      'omega_kickstart' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'omega_kickstart',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  $export['views-0541d3342c37e8cfbaa4c69181992b70'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => '0541d3342c37e8cfbaa4c69181992b70',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'users/*
user',
    'roles' => array(),
    'themes' => array(
      'adaptivetheme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adaptivetheme',
        'weight' => 0,
      ),
      'at_commerce' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'at_commerce',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'fps' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'fps',
        'weight' => 5,
      ),
      'omega_kickstart' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'omega_kickstart',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-1282d79b9edcf69589eaa08b43df156d'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => '1282d79b9edcf69589eaa08b43df156d',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'user
users/*',
    'roles' => array(),
    'themes' => array(
      'adaptivetheme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adaptivetheme',
        'weight' => 0,
      ),
      'at_commerce' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'at_commerce',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'fps' => array(
        'region' => 'sidebar_second',
        'status' => 1,
        'theme' => 'fps',
        'weight' => -28,
      ),
      'omega_kickstart' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'omega_kickstart',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-4c8340b29778061eb369e385b21ec489'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => '4c8340b29778061eb369e385b21ec489',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adaptivetheme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adaptivetheme',
        'weight' => 0,
      ),
      'at_commerce' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'at_commerce',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'fps' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'fps',
        'weight' => 6,
      ),
      'omega_kickstart' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'omega_kickstart',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-a2bbec217e3abf224c5a6fa6676c5d62'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'a2bbec217e3abf224c5a6fa6676c5d62',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'user/*/orders/*',
    'roles' => array(),
    'themes' => array(
      'adaptivetheme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adaptivetheme',
        'weight' => 0,
      ),
      'at_commerce' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'at_commerce',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'fps' => array(
        'region' => 'content_aside',
        'status' => 1,
        'theme' => 'fps',
        'weight' => 0,
      ),
      'omega_kickstart' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'omega_kickstart',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 1,
  );

  $export['views-be17b23bee26742dc9633378e9042c8c'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'be17b23bee26742dc9633378e9042c8c',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'user
users/*',
    'roles' => array(),
    'themes' => array(
      'adaptivetheme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adaptivetheme',
        'weight' => 0,
      ),
      'at_commerce' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'at_commerce',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'fps' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'fps',
        'weight' => -27,
      ),
      'omega_kickstart' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'omega_kickstart',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-c24b21c58aed8cc449729d9d08d07159'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'c24b21c58aed8cc449729d9d08d07159',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'user
users/*',
    'roles' => array(),
    'themes' => array(
      'adaptivetheme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adaptivetheme',
        'weight' => 0,
      ),
      'at_commerce' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'at_commerce',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'fps' => array(
        'region' => 'sidebar_second',
        'status' => 1,
        'theme' => 'fps',
        'weight' => -29,
      ),
      'omega_kickstart' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'omega_kickstart',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-current_license-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'current_license-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'user
users/*',
    'roles' => array(),
    'themes' => array(
      'adaptivetheme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adaptivetheme',
        'weight' => 0,
      ),
      'at_commerce' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'at_commerce',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'fps' => array(
        'region' => 'sidebar_second',
        'status' => 1,
        'theme' => 'fps',
        'weight' => -29,
      ),
      'omega_kickstart' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'omega_kickstart',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-current_license-block_1'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'current_license-block_1',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'user
users/*',
    'roles' => array(),
    'themes' => array(
      'adaptivetheme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adaptivetheme',
        'weight' => 0,
      ),
      'at_commerce' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'at_commerce',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'fps' => array(
        'region' => 'sidebar_second',
        'status' => 1,
        'theme' => 'fps',
        'weight' => -28,
      ),
      'omega_kickstart' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'omega_kickstart',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-fillpdf_service_api_usage-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'fillpdf_service_api_usage-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'user
users/*',
    'roles' => array(),
    'themes' => array(
      'adaptivetheme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adaptivetheme',
        'weight' => 0,
      ),
      'at_commerce' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'at_commerce',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'fps' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'fps',
        'weight' => -28,
      ),
      'omega_kickstart' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'omega_kickstart',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-fps_shopping_cart-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'fps_shopping_cart-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adaptivetheme' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'adaptivetheme',
        'weight' => 0,
      ),
      'at_commerce' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'at_commerce',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'fps' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'fps',
        'weight' => 7,
      ),
      'omega_kickstart' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'omega_kickstart',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['views-shopping_cart-block'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'shopping_cart-block',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'cart\\ncheckout/*',
    'roles' => array(),
    'themes' => array(
      'adaptivetheme' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'adaptivetheme',
        'weight' => 0,
      ),
      'at_commerce' => array(
        'region' => 'sidebar_first',
        'status' => 1,
        'theme' => 'at_commerce',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'fps' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'fps',
        'weight' => 8,
      ),
      'omega_kickstart' => array(
        'region' => 'user_second',
        'status' => 1,
        'theme' => 'omega_kickstart',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  return $export;
}
