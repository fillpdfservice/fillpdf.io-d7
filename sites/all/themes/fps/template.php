<?php

/**
 * @file
 * Template.php - process theme data for your sub-theme.
 *
 * Rename each function and instance of "footheme" to match
 * your subthemes name, e.g. if you name your theme "footheme" then the function
 * name will be "footheme_preprocess_hook". Tip - you can search/replace
 * on "footheme".
 */
const VAT_NUMBER = '999 052 835 MVA';

/**
 * Override or insert variables for the page templates.
 */
function fps_process_page(&$vars) {
  // Get rid of AT attribution.
  $vars['attribution'] = '';
}

function fps_preprocess_commerce_billy_pdf_page(&$variables) {
  if (empty($variables['viewed_orders'])) {
    return;
  }

  foreach ($variables['viewed_orders'] as &$viewed_order) {
    foreach ($viewed_order as &$index) {
      foreach ($index as &$entity) {
        // If VAT was charged, show VAT number.
        $has_vat = FALSE;
        if (empty($entity['commerce_order_total']['#items'][0]['data']['components'])) {
          continue;
        }
        foreach ($entity['commerce_order_total']['#items'][0]['data']['components'] as $component) {
          if (substr($component['name'], 0, 4) === 'vat|') {
            $has_vat = TRUE;
          }
        }

        if ($has_vat) {
          $entity['invoice_text']['#markup'] .= t('Our VAT number: @vat_number<br />', ['@vat_number' => VAT_NUMBER]);
          continue;
        }

        $entity['invoice_text']['#markup'] .= t('Reverse charge/B2B transaction<br />');
        $entity['invoice_text']['#markup'] .= t('Our VAT number: @vat_number<br />', ['@vat_number' => VAT_NUMBER]);
      }
    }
  }
}
